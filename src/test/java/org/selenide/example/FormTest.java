package org.selenide.example;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class FormTest {
    @Test
    public void testOpenForm() {
        open("https://form.jotformeu.com/92543326450353");
        $(By.id("jfCard-welcome-start")).waitUntil(Condition.visible, 10000);
        $(By.id("jfCard-welcome-start")).click();
    }
}
